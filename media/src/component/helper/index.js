import Control from './control.vue'
import Image from './image.vue'
import Link from './link.vue'
import Tinymce from './tinymce.vue'
import Partial from './partial.vue'

Vue.prototype.$helper.component(Control)
Vue.prototype.$helper.component(Image)
Vue.prototype.$helper.component(Link)
Vue.prototype.$helper.component(Tinymce)
Vue.prototype.$helper.component(Partial)