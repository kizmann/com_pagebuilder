import Body from './body.vue'
import BodyPosition from './body-position.vue'
import BodyPage from './body-page.vue'
import BodyPartial from './body-partial.vue'
Vue.prototype.$helper.component(Body)
Vue.prototype.$helper.component(BodyPosition)
Vue.prototype.$helper.component(BodyPage)
Vue.prototype.$helper.component(BodyPartial)
