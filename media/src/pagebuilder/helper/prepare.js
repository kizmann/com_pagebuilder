function prepare (defaults, value) {

    if ( _.isPlainObject(defaults) && _.isArray(value) && value.length === 0 ) {
        value = {}
    }
    
    if ( _.isArray(defaults) ) {
        return _.map(value, (item, index) => {
            return prepare(defaults[0], item)
        })
    }

    if ( _.isPlainObject(defaults) ) {
        return _.mapValues(defaults, (item, index) => {
            return prepare(item, value[index])
        })
    }

    if ( _.isString(defaults) && value === undefined ) {
        return defaults
    }

    if ( _.isString(defaults) && value !== undefined ){
        return String(value)
    }

    if ( _.isNumber(defaults) && value === undefined ) {
        return defaults
    }

    if ( _.isNumber(defaults) && value !== undefined ){
        return Number(value)
    }

    if ( _.isBoolean(defaults) && value === undefined ) {
        return defaults
    } 
    
    if ( _.isBoolean(defaults) && value !== undefined ){
        return value === 'false' ? false : true
    }

    return value
}

export default prepare