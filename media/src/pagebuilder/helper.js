
import Prepare from './helper/prepare.js'

export default function PagebuilderHelper(Vue) {

    Vue.prototype.$helper = {
        app: {
            el: '[data-nano]'
        },
        component: (component) => {
            Vue.component(component.name, component)
        },
        prepare: Prepare
    }

}

if (Vue) {
    Vue.use(PagebuilderHelper)
}
