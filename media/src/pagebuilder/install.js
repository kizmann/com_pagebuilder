
import $ from 'jquery'
import _ from 'lodash'
import VueI18n from 'vue-i18n'
import ElementUI from 'element-ui'
import ElementUIenLocale from 'element-ui/lib/locale/lang/en'
import ElementUIdeLocale from 'element-ui/lib/locale/lang/de'
import Draggable from 'vuedraggable'

import Store from '../store/index.js'
import Helper from '../component/helper/index.js'
import Body from '../component/body/index.js'
import Sidebar from '../component/sidebar/index.js'
import Menu from '../component/menu/index.js'
import Toolbar from '../component/toolbar/index.js'
import Filter from '../component/filter/index.js'

export default function PagebuilderInstall (Vue) {

    $(() => {

        Vue.use(VueI18n)

        const messages = { 
            en: { ElementUIenLocale },
            de: { ElementUIdeLocale }
        }

        Vue.prototype.$helper.app.i18n = new VueI18n({ locale: window.lang, messages })

        Vue.use(ElementUI, {
            i18n: (key, value) => Vue.prototype.$helper.app.i18n.t(key. value)
        })

        Vue.prototype.$helper.app.mounted = function () {
            this.$store.dispatch('State/request')
            this.$store.dispatch('Lang/request')
            this.$store.dispatch('Image/request')
            this.$store.dispatch('MenuType/request')
            this.$store.dispatch('Menu/request')
            this.$store.dispatch('Category/request')
            this.$store.dispatch('Module/request')
            this.$store.dispatch('Filter/request')
            this.$store.dispatch('Page/index')
            this.$store.dispatch('Partial/index')
        }

        Draggable.name = 'pb-drag'
        Vue.prototype.$helper.component(Draggable)
    
        window.App = new Vue(Vue.prototype.$helper.app)

    })

}

if (Vue) {
    Vue.use(PagebuilderInstall)
}