import VueResource from 'vue-resource'

export default function PagebuilderRequest (Vue) {

    const API_URL = '/administrator/index.php?option=com_pagebuilder'

    Vue.http.options.emulateJSON = true;

    var $queue = {
        loading: false, count: 0, instance: null
    }

    var start = (request, next) => {
        
        if (
            $queue.loading === false &&
            $queue.count === 0
        ) {
            $queue.loading = true
            $queue.instance = Vue.prototype.$loading({ fullscreen: true })
        }

        $queue.count++
        next(stop)
    }

    var stop = (request) => {

        if (
            $queue.loading === true &&
            $queue.count === 1
        ) {
            $queue.loading = false
            $queue.instance.close()
        }

        $queue.count--
    }

    Vue.http.interceptors.push(start)

    var error = (response, status) => {
        Vue.prototype.$notify.error({
            title: response.status + ' ' + response.statusText,
            message: response.body.message ? response.body.message : 'An unexpected error occured.'
        })
    }

    var url = (task) => {
        return API_URL + '&view=' + task.split('.')[0] + '&task=' + task.split('.')[1]
    }

    Vue.prototype.$request = (task, data, callback) => {
        var api_url = url(task)
        return Vue.http.post(api_url, data).then(callback, error)
    }

}

if (Vue) {
    Vue.use(VueResource)
    Vue.use(PagebuilderRequest)
}
