export default {
    namespaced: true,
    state: {
        items: [],
        buffer: null
    },
    mutations: {
        init: (state, value) => {
            state.items = value
        },
        buffer: (state, value) => {
            state.buffer = value
        }
    },
    actions: {
        buffer: ({ commit, state }, value) => {
            
            if ( state.buffer !== null && state.buffer !== undefined ) {
                return
            }

            if ( value.id !== 0 ) {
                Vue.prototype.$request('partial.show', value, (response) => {
                    commit('buffer', response.body)
                })
            } else {
                Vue.prototype.$request('partial.buffer', {}, (response) => {
                    commit('buffer', response.body)
                })
            }

            return
        },
        index: ({ commit, state }, value) => {
            Vue.prototype.$request('partial.index', {}, (response) => {
                commit('init', response.body)
            })
        },
        create: ({ commit, state }, value) => {
            Vue.prototype.$request('partial.create', state.buffer, (response) => {
                window.location.assign(response.body.updateLink)
            })
        },
        save: ({ commit, state }, value) => {
            Vue.prototype.$request('partial.save', state.buffer, (response) => {
                
            })
        },
        close: ({ commit, state }, value) => {
            Vue.prototype.$request('partial.save', state.buffer, (response) => {
                window.location.assign('index.php?option=com_pagebuilder&view=partial')
            })
        },
        copy: ({ commit, state }, value) => {
            Vue.prototype.$request('partial.copy', state.buffer, (response) => {
                window.location.assign(response.body.updateLink)
            })
        },
        remove: ({ commit, state }, value) => {
            Vue.prototype.$request('partial.remove', state.buffer, (response) => {
                window.location.assign('index.php?option=com_pagebuilder&view=partial')
            })
        }
    }
}