export default {
    namespaced: true,
    state: {
        items: []
    },
    mutations: {
        update: (state, value) => {
            state.items = value
        }
    },
    actions: {
        request: ({ commit, state }, value) => {
            Vue.prototype.$request('joomla.module', {}, (response) => {
                commit('update', response.body)
            })
        }
    }
}