export default {
    namespaced: true,
    state: {
        items: []
    },
    mutations: {
        update: (state, value) => {
            state.items = value
        }
    },
    actions: {
        request: ({ commit, state }, value) => {
            Vue.prototype.$request('joomla.lang', {}, (response) => {
                commit('update', response.body)
            })
        }
    }
}