
import Vuex from 'vuex'

import State from './state.js'
import Lang from './lang.js'
import Image from './image.js'
import MenuType from './menutype.js'
import Menu from './menu.js'
import Category from './category.js'
import Module from './module.js'
import Filter from './filter.js'
import Page from './page.js'
import Partial from './partial.js'

if ( Vue ) {
    Vue.prototype.$helper.app.store = new Vuex.Store({
        modules: { State, Lang, Image, MenuType, Menu, Category, Module, Filter, Page, Partial }
    })
}