export default {
    namespaced: true,
    state: {
        items: [],
        buffer: null
    },
    mutations: {
        items: (state, value) => {
            state.items = value
        },
        buffer: (state, value) => {
            state.buffer = value
        }
    },
    actions: {
        buffer: ({ commit, state }, value) => {

            if ( state.buffer !== null && state.buffer !== undefined ) {
                return
            }

            if ( value.id !== 0 ) {
                Vue.prototype.$request('page.show', value, (response) => {
                    commit('buffer', response.body)
                })
            } else {
                Vue.prototype.$request('page.buffer', {}, (response) => {
                    commit('buffer', response.body)
                })
            }

            return
        },
        index: ({ commit, state }, value) => {
            Vue.prototype.$request('page.index', {}, (response) => {
                commit('items', response.body)
            })
        },
        create: ({ commit, state }, value) => {
            Vue.prototype.$request('page.create', state.buffer, (response) => {
                window.location.assign(response.body.updateLink)
            })
        },
        save: ({ commit, state }, value) => {
            Vue.prototype.$request('page.save', state.buffer, (response) => {
                
            })
        },
        close: ({ commit, state }, value) => {
            Vue.prototype.$request('page.save', state.buffer, (response) => {
                window.location.assign('index.php?option=com_pagebuilder&view=page')
            })
        },
        copy: ({ commit, state }, value) => {
            Vue.prototype.$request('page.copy', state.buffer, (response) => {
                window.location.assign(response.body.updateLink)
            })
        },
        remove: ({ commit, state }, value) => {
            Vue.prototype.$request('page.remove', state.buffer, (response) => {
                window.location.assign('index.php?option=com_pagebuilder&view=page')
            })
        }
    }
}