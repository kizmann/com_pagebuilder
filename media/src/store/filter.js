export default {
    namespaced: true,
    state: {
        search: '',
        state: [],
        lang: [],
        page: '1',
        limit: '25',
        category: []
    },
    mutations: {
        update: (state, value) => {
            _.extend(state, value)
        },
        updateSearch: (state, value) => {
            state.search = value
        },
        updateState: (state, value) => {
            state.state = value
        },
        updateLang: (state, value) => {
            state.lang = value
        },
        updatePage: (state, value) => {
            state.page = value
        },
        updateLimit: (state, value) => {
            state.limit = value
        }
    },
    actions: {
        request: ({ commit, state }, value) => {
            Vue.prototype.$request('filter.request', {}, (response) => {
                commit('update', response.body)
            })
        },
        update: ({ commit, state }, value) => {
            Vue.prototype.$request('filter.update', { body: value }, (response) => {

                Vue.prototype.$message({
                    type: 'success',
                    message: 'Filter updated'
                })

                commit('update', response.body)
            })
        },
        reset: ({ commit, state }, value) => {
            Vue.prototype.$request('filter.reset', {}, (response) => {

                Vue.prototype.$message({
                    type: 'info',
                    message: 'Filter reseted'
                })

                commit('update', response.body)
            })
        }
    }
}