const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const ConcatPlugin = require('webpack-concat-plugin');

module.exports = {

    entry: {
        'dist/vendor': [
            'lodash/lodash.js'
        ],
        'dist/index': [
            './src/index.scss', './src/index.js'
        ]
    },
    output: {
        path: __dirname, filename: "[name].js"
    },
    module: {
        rules: [
            { test: /\.vue$/, loader: 'vue-loader', exclude: /node_modules/ },
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /\.scss$/, use: ExtractTextPlugin.extract(['css-loader', 'sass-loader']) },
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {}
        }),
        new ExtractTextPlugin('dist/index.css')
    ],
    externals: {
        "jquery": "jQuery",
        "lodash": "_",
        "vue": "Vue"
    }
}