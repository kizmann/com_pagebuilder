<?php

namespace Pagebuilder\Controller\Ajax;

use Pagebuilder\Model\MenuType as MenuTypeModel;
use Pagebuilder\Model\Menu as MenuModel;
use Pagebuilder\Model\Category as CategoryModel;

use Pagebuilder\Helper\Config as ConfigHelper;
use Pagebuilder\Helper\State as StateHelper;
use Pagebuilder\Helper\Lang as LangHelper;
use Pagebuilder\Helper\Image as ImageHelper;

class Joomla {

    public function Module()
    {
        $body = (new ConfigHelper)->module();
        return view()->json($body);
    }

    public function Image()
    {
        $body = (new ImageHelper)->get();
        return view()->json($body);
    }

    public function State()
    {
        $body = (new StateHelper)->get();
        return view()->json($body);
    }

    public function Lang()
    {
        $body = (new LangHelper)->get();
        return view()->json($body);
    }

    public function MenuType()
    {
        $body = (new MenuTypeModel)->with(['menuBackend'])->orderBy('id', 'desc')->get(['id', 'menutype', 'title']);
        return view()->json($body);
    }

    public function Menu()
    {
        $body = (new MenuModel)->orderBy('lft', 'asc')->get(['id', 'menutype', 'title', 'alias', 'link', 'params', 'published']);
        return view()->json($body);
    }

    public function Category()
    {
        $body = (new CategoryModel)->where('extension', 'com_content')->orderBy('lft', 'asc')->get(['id', 'parent_id', 'extension', 'title', 'published']);
        return view()->json($body);
    }

}