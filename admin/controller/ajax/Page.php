<?php

namespace Pagebuilder\Controller\Ajax;

use Nano\Component\Controller as Controller;
use Pagebuilder\Model\Page as PageModel;
use Exception as Exception;

class Page extends Controller
{

    public function index()
    {
        $model = new PageModel;
        return view()->json($model->orderBy('id', 'desc')->get(['id', 'state', 'lang', 'category', 'title', 'created_at', 'updated_at']), 200);
    }

    public function show()
    {
        $model = new PageModel;
        
        $id = request()->get('id', 0);

        if ( ! $id ) {
            $message = trans('com_pagebuilder_error_id_required');
            throw new Exception($message, 500);
        }

        $model = (new PageModel)->find($id);

        if ( ! $model ) {
            $message = trans('com_pagebuilder_error_page_not_found');
            throw new Exception($message, 500);
        }

        return view()->json($model, 200);
    }

    public function buffer()
    {
        $model = new PageModel;
        return view()->json($model, 200);
    }

    public function create()
    {       
        $model = new PageModel;

        $model->fill(request()->only(
            $model->getFillable()
        ))->save();

        foreach($model->errors()->all() as $error) {
            throw new Exception(trans($error), 400);
        }

        return view()->json($model, 200);
    }

    public function save()
    {
        $model = new PageModel;

        $id = request()->get('id', 0);

        if ( ! $id ) {
            $message = trans('com_pagebuilder_error_id_required');
            throw new Exception($message, 500);
        }

        $model = (new PageModel)->find($id);

        if ( ! $model ) {
            $message = trans('com_pagebuilder_error_page_not_found');
            throw new Exception($message, 500);
        }

        $model->fill(request()->only(
            $model->getFillable()
        ))->save();

		foreach($model->errors()->all() as $error) {
			throw new Exception(trans($error), 400);
        }

        return view()->json($model, 200);
    }

    public function copy()
    {       
        $model = new PageModel;
        
        $id = request()->get('id', 0);

        if ( ! $id ) {
            $message = trans('com_pagebuilder_error_id_required');
            throw new Exception($message, 500);
        }

        $model = (new PageModel)->find($id);

        if ( ! $model ) {
            $message = trans('com_pagebuilder_error_page_not_found');
            throw new Exception($message, 500);
        }

        $clone = $model->replicate();

        $clone->fill([
            'title' => preg_replace('/\s\-\s[0-9]+\:[0-9]+\:[0-9]+\s[0-9]+\.[0-9]+\.[0-9]+$/', '', $clone->title) . ' - ' . date('H:i:s d.m.y')
        ])->save();

        return view()->json($clone, 200);
    }

    public function remove()
    {       
        $model = new PageModel;
        
        $id = request()->get('id', 0);

        if ( ! $id ) {
            $message = trans('com_pagebuilder_error_id_required');
            throw new Exception($message, 500);
        }

        $model = (new PageModel)->find($id);

        if ( ! $model ) {
            $message = trans('com_pagebuilder_error_page_not_found');
            throw new Exception($message, 500);
        }

        $model->delete();

        return view()->json([], 200);
    }

}