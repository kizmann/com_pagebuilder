<?php

namespace Pagebuilder\Controller\Ajax;

class Filter {

    public $default = [
        'search'    => '',
        'state'     => ['1'],
        'lang'      => ['*'],
        'page'      => 1,
        'limit'     => 25,
        'category'  => []
    ];

    public function request()
    {
        $body = request()->get('body', [], 'array');

        $body = collect(array_merge($this->default, $body))->map(function($item, $index) {
            return state()->get($index, $item);
        });

        return view()->json($body, 200);
    }

    public function update()
    {
        $body = request()->get('body', []);

        $body = collect(array_merge($this->default, $body))->map(function($item, $index) {
            return state()->set($index, $item ? $item : $this->default[$index]);
        });

        return view()->json($body, 200);
    }

    public function reset()
    {
        $body = request()->get('body', []);
        
        $body = collect(array_merge($this->default, $body))->map(function($item, $index) {
            return state()->set($index, $this->default[$index]);
        });

        return view()->json($body, 200);
    }

}