<?

namespace Pagebuilder\Controller\Site;

use Nano\Component\Controller as Controller;
use Pagebuilder\Model\Page as PageModel;

class Page extends Controller
{

    public function index()
    {
        $model = new PageModel;

        return view()->render('page/tmpl/index', [
            'item' => $model->findOrFail(app()->input->get('id', 0))
        ]);
    }

}