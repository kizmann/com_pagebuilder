<?

namespace Pagebuilder\Controller\Admin;

use Nano\Component\Controller as Controller;
use Pagebuilder\Helper\Config as ConfigHelper;

class Page extends Controller
{

    public function __construct()
    {
        parent::__construct();
        ConfigHelper::make()->append();
    }

    public function index()
    {
        if ( ! user()->authorise('core.manage', 'com_pagebuilder') ) {
            throw new Exception(trans('com_pagebuilder_error_access'));
        }

        view()->render('page/tmpl/index', []);
    }

    public function modal()
    {
        if ( ! user()->authorise('core.manage', 'com_pagebuilder') ) {
            throw new Exception(trans('com_pagebuilder_error_access'));
        }

        view()->render('page/tmpl/modal', []);
    }

    public function create()
    {
        if ( ! user()->authorise('core.manage', 'com_pagebuilder') ) {
            throw new Exception(trans('com_pagebuilder_error_access'));
        }

        view()->render('page/tmpl/create', []);
    }

    public function update()
    {
        if ( ! user()->authorise('core.manage', 'com_pagebuilder') ) {
            throw new Exception(trans('com_pagebuilder_error_access'));
        }

        view()->render('page/tmpl/update', []);
    }

}