<?php

namespace Pagebuilder\Helper;

use Illuminate\Database\Capsule\Manager as DB;

class Template {

    public $template;

    public static function make()
    {
        return new Template;
    }

    public function __construct()
    {
        $this->template = DB::table('template_styles')->where('client_id', 0)->where('home', 1)->first();
    }

    public function get($key)
    {
        return $this->template->{$key};
    }

    public function getTemplate()
    {
        return $this->template->template;
    }

    public function getTemplateDir()
    {
        return rtrim(JPATH_ROOT, '/') . '/' . 'templates' . '/' . trim($this->getTemplate(), '/');
    }

    public function getTemplateUrl()
    {
        return rtrim(rootURL(), '/') . '/' . 'templates' . '/' . trim($this->getTemplate(), '/');
    }

    public function render($file, $vars = [])
    {
        call_user_func(function() use ($file, $vars) {
            extract($vars); include $this->getTemplateDir() . '/' . ltrim($file, '/');
        });
    }

}