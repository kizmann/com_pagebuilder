<?php

namespace Pagebuilder\Helper;

jimport('joomla.filesystem.folder');

use Illuminate\Database\Capsule\Manager as DB;
use Pagebuilder\Helper\Template as Template;
use JFolder as JFolder;

class Config
{
    public $base = 'pagebuilder';

    public $dir = '';
    public $url = '';

    public static function make()
    {
        return new Config;
    }

    public function __construct()
    {
        $this->dir = Template::make()->getTemplateDir();
        $this->url = Template::make()->getTemplateUrl();
    }

    public function module()
    {
        $path = $this->dir . '/' . $this->base . '/config.php';

        if ( ! file_exists($path) ) {
            return [];
        }

        $config = include($path);

        foreach ( $config as $index => $module ) {
            $config[$index] = toObject($module);
        }

        return $config;
    }

    public function script()
    {
        $path = $this->dir . '/' . $this->base . '/script.php';

        if ( ! file_exists($path) ) {
            return [];
        }

        return include($path);
    }

    public function style()
    {
        $path = $this->dir . '/' . $this->base . '/style.php';

        if ( ! file_exists($path) ) {
            return [];
        }

        return include($path);
    }

    public function append()
    {
        foreach ( $this->style() as $item ) {

            if ( ! is_string($item) ) {
                return;
            }

            doc()->addStylesheet(str_replace('%TPL%', $this->url  . '/' . $this->base, $item));
        }

        foreach ( $this->script() as $item ) {

            if ( ! is_string($item) ) {
                return;
            }

            doc()->addScript(str_replace('%TPL%', $this->url  . '/' . $this->base, $item));
        }

        foreach ( $this->module() as $item ) {

            if ( ! isset($item->script) || ! is_string($item->script) ) {
                return;
            }

            doc()->addScript($this->url . '/' . $this->base . $item->script);
        }

        return $this;
    }

    public function render($type, $vars)
    {
        $module = collect($this->module())->first(function($index, $item) use ($type) {
            return $item->value === $type;
        });

        if ( ! $module ) {
            return;
        }

        return Template::make()->render('/' . $this->base . $module->entry, $vars);
    }

}