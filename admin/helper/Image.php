<?php

namespace Pagebuilder\Helper;

jimport('joomla.filesystem.folder');
use JFolder as JFolder;

class Image
{

    public $dir = '';
    public $folder = [];

    public static function make()
    {
        return new Image;
    }

    public function __construct()
    {
        $this->dir = JPATH_ROOT . '/images';
    }

    public function get()
    {
        $files = JFolder::files($this->dir, null, true, true);

        collect($files)->map(function($item) {
            // Prepare item
            $item = str_replace($this->dir, '', $item);
            // Register item to folder
            $this->folder = $this->register($this->folder, $item);
        });

        return $this->folder;
    }

    public function register($array, $item, $delimiter = '/')
    {
        // Create array buffer
        $buffer = &$array;

        // Explode file path
        $keys = explode($delimiter, ltrim($item, $delimiter));

        // Get last item and save as value
        $value = array_pop($keys);

        // Loop if keys exists
        if ( $keys ) {
            foreach ($keys as $key) {
                $buffer = &$buffer[$key];
            }
        }

        // Assign buffer
        $buffer[$value] = '/images' . $item;

        return $array;
    }

}