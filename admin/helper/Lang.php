<?php

namespace Pagebuilder\Helper;

use Illuminate\Database\Capsule\Manager as DB;

class Lang
{

    public $options = [];

    public static function make ()
    {
        return new Lang;
    }

    public function __construct ()
    {
        DB::table('languages');
        
        $this->options[] = ['label' => trans('jall_language'), 'value' => '*'];

        foreach (DB::table('languages')->get() as $option) {
            $this->options[] = ['label' => $option->title, 'value' => $option->lang_code];
        }
    }

    public function get()
    {
        return $this->options;
    }

    public function collection()
    {
        return collect($this->options);
    }

    public function find($value, $key = 'value')
    {
        $result = $this->collection()->filter(function($option) use ($key, $value) {
            return (string) $option[$key] === (string) $value;
        });

        return $result->first();
    }

}