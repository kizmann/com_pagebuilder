<?php

namespace Pagebuilder\Helper;

class State
{

    public $options = [];

    public static function make ()
    {
        return new State;
    }

    public function __construct ()
    {
        $this->options = [
            [
                'label' => trans('jpublished'),
                'value' => '1',
                'type'  => 'success'
            ],
            [
                'label' => trans('junpublished'),
                'value' => '0',
                'type'  => 'danger'
            ],
            [
                'label' => trans('jarchived'),
                'value' => '2',
                'type'  => 'warning'
            ],
            [
                'label' => trans('jtrashed'),
                'value' => '-2',
                'type'  => 'gray'
            ]
        ];
    }

    public function get()
    {
        return $this->options;
    }

    public function collection()
    {
        return collect($this->options);
    }

    public function find($value, $key = 'value')
    {
        $result = $this->collection()->filter(function($option) use ($key, $value) {
            return (string) $option[$key] === (string) $value;
        });

        return $result->first();
    }

}