<?php

if( ! defined('NANO_VERSION') || NANO_VERSION < 2 ) {
    throw new Exception('Nano Framework version 2.x is required.');
}

use Nano\Component\Loader as Loader;
use Nano\Component\Factory as Factory;

include_once('install/pages.php');
include_once('install/partials.php');

(new Loader)->addPrefix('Pagebuilder', JPATH_COMPONENT_ADMINISTRATOR)->register();

// Remove mootools
$head = doc()->getHeadData();
$scripts = $head['scripts'];

unset($scripts['/media/system/js/mootools-core.js']);
unset($scripts['/media/system/js/mootools-more.js']);
unset($scripts['/media/system/js/mootools-core-uncompressed.js']);
unset($scripts['/media/system/js/mootools-more-uncompressed.js']);

$head['scripts'] = $scripts;
doc()->setHeadData($head);

(new Factory)->boot(['pagebuilder'], ['page']);