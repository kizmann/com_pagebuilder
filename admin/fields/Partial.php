<?php

defined('JPATH_BASE') or die;

use Nano\Component\Loader as Loader;
use Pagebuilder\Model\Partial as PartialModel;

class JFormFieldPartial extends JFormField
{
    protected $type = 'parial';

    public function __construct()
    {
        (new Loader)->addPrefix('Pagebuilder', JPATH_ADMINISTRATOR . '/components/com_pagebuilder')->register();
    }

    protected function getInput()
    {
        // Load language
        lang()->load('com_pagebuilder', JPATH_ADMINISTRATOR . '/components/com_pagebuilder');

        // Add modal behavior
        JHtml::_('behavior.modal', 'a.modal');

        // Define modal link
        $link = 'index.php?option=com_pagebuilder&view=partial&task=modal&tmpl=component';

        // Get partial by id
        $partial = PartialModel::find($this->value);

        // Set default
        $this->valueId = '';
        $this->valueName = '';

        if ( $partial ) {
            $this->valueId = $partial->id;
            $this->valueName = $partial->title;
        }

        $script = '';
        $script .= 'function selectPartial(id, name) {';
        $script .= 'document.id("pb_partial_id").value = id;';
        $script .= 'document.id("pb_partial_name").value = name;';
        $script .= 'SqueezeBox.close();';
        $script .= '}';

        // Add script
        doc()->addScriptDeclaration($script);

        $output = '';
        $output .= '<div class="row-fluid">';
        $output .= '<div class="input-append pull-left">';
        $output .= '<input type="text" id="pb_partial_name" readonly="readonly" disabled="disabled" class="input-medium" placeholder="' . trans('com_pagebuilderpartial_placeholder') . '" value="' . $this->valueName . '" />';
        $output .= '<a class="modal btn btn-primary" href="' . $link . '" title="' . trans('com_pagebuilder_partial_select') . '" rel="{handler: \'iframe\', size: {x:1280, y:640}}"><span class="icon-tags" style="font-size: 0.8em; margin-right: 7px;"></span>' . trans('com_pagebuilder_partial_select') . '</a>';
        $output .= '</div>';

        if ( $partial ) {
            $output .= '<div class="pull-left" style="margin-left: 10px;">';
            $output .= '<a class="btn" target="_blank" href="' . $partial->updateLink . '"><span class="icon-link" style="font-size: 0.8em; margin-right: 7px;"></span>' . $partial->title . '</a>';
            $output .= '</div>';
        }

        $output .= '</div>';
        $output .= '<input type="hidden" id="pb_partial_id" name="' . $this->name . '" value="' . $this->valueId . '" />';

        return $output;
    }

}