<?
    toolbar()->title(
        trans('com_pagebuilder_partials')
    );
?>
<div class="pb-reset grid-flex" data-nano="true" data-backend="true">
    <pb-body>
        <template slot="filter">
            <pb-filter :modal="true"></pb-filter>
        </template>
        <template>
            <h2><?= trans('com_pagebuilder_partials'); ?></h2>
            <pb-filter-body v-model="$store.state.Partial.items" :modal="true" modal-function="selectPartial"></pb-filter-body>
        </template>
    </pb-body>
</div>