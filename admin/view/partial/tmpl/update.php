<?
    toolbar()->title(
        trans('com_pagebuilder_partial_update')
    );
?>
<div class="pb-reset grid-flex" data-nano="true" data-backend="true">
    <pb-sidebar>
        <? include(JPATH_COMPONENT . '/view/menu.php'); ?>
    </pb-sidebar>
    <pb-body>
        <pb-toolbar slot="toolbar">
            <pb-toolbar-item @click="$store.dispatch('Partial/save')" icon="icon-save" value="<?= trans('com_pagebuilder_partial_save'); ?>" css="green"></pb-toolbar-item>
            <pb-toolbar-item @click="$store.dispatch('Partial/close')" icon="icon-save" value="<?= trans('com_pagebuilder_partial_save_close'); ?>" css="green"></pb-toolbar-item>
            <pb-toolbar-item link="index.php?option=com_pagebuilder&amp;view=partial" icon="icon-remove" value="<?= trans('com_pagebuilder_partial_close'); ?>" css="blue"></pb-toolbar-item>
            <pb-toolbar-item @click="$store.dispatch('Partial/copy')" icon="icon-copy" value="<?= trans('com_pagebuilder_partial_copy'); ?>" css="yellow pull-right"></pb-toolbar-item>
            <pb-toolbar-item @click="$store.dispatch('Partial/remove')" icon="icon-remove" value="<?= trans('com_pagebuilder_partial_remove'); ?>" css="red" confirm="<?= trans('com_pagebuilder_partial_remove_confirm'); ?>"></pb-toolbar-item>
        </pb-toolbar>
        <template>
            <h1><?= trans('com_pagebuilder_partial_update'); ?></h1>
            <pb-body-partial id="<?= request()->get('itemid', ''); ?>"></pb-body-partial>
        </template>
    </pb-body>
</div>