<?
    toolbar()->title(
        trans('com_pagebuilder_partial_create')
    );
?>
<div class="pb-reset grid-flex" data-nano="true" data-backend="true">
    <pb-sidebar>
        <? include(JPATH_COMPONENT . '/view/menu.php'); ?>
    </pb-sidebar>
    <pb-body>
        <pb-toolbar slot="toolbar">
            <pb-toolbar-item @click="$store.dispatch('Partial/create')" icon="icon-save" value="<?= trans('com_pagebuilder_partial_save'); ?>" css="green"></pb-toolbar-item>
        </pb-toolbar>
        <template>
            <h1><?= trans('com_pagebuilder_partial_create'); ?></h1>
            <pb-body-partial></pb-body-partial>
        </template>
    </pb-body>
</div>