<pb-menu>
    <pb-menu-item link="<?= route('index.php?option=com_pagebuilder&view=page') ?>" icon="icon-grid"></pb-menu-item>
    <pb-menu-item link="<?= route('index.php?option=com_menus&view=items') ?>" icon="icon-list" value="<?= trans('com_pagebuilder_menus'); ?>"></pb-menu-item>
    <pb-menu-item link="<?= route('index.php?option=com_pagebuilder&view=page') ?>" icon="icon-file" value="<?= trans('com_pagebuilder_pages'); ?>"></pb-menu-item>
    <pb-menu-item link="<?= route('index.php?option=com_pagebuilder&view=partial') ?>" icon="icon-tags" value="<?= trans('com_pagebuilder_partials'); ?>"></pb-menu-item>
</pb-menu>