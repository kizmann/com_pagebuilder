<?
    toolbar()->title(
        trans('com_pagebuilder_page_update')
    );
?>
<div class="pb-reset grid-flex" data-nano="true" data-backend="true">
    <pb-sidebar>
        <? include(JPATH_COMPONENT . '/view/menu.php'); ?>
    </pb-sidebar>
    <pb-body>
        <pb-toolbar slot="toolbar">
			<pb-toolbar-item @click="$store.dispatch('Page/save')" icon="icon-save" value="<?= trans('com_pagebuilder_page_save'); ?>" css="green"></pb-toolbar-item>
			<pb-toolbar-item @click="$store.dispatch('Page/close')" icon="icon-save" value="<?= trans('com_pagebuilder_page_save_close'); ?>" css="green"></pb-toolbar-item>
            <pb-toolbar-item link="index.php?option=com_pagebuilder&amp;view=page" icon="icon-remove" value="<?= trans('com_pagebuilder_page_close'); ?>" css="blue"></pb-toolbar-item>
            <pb-toolbar-item @click="$store.dispatch('Page/copy')" icon="icon-copy" value="<?= trans('com_pagebuilder_page_copy'); ?>" css="yellow pull-right"></pb-toolbar-item>
			<pb-toolbar-item @click="$store.dispatch('Page/remove')" icon="icon-remove" value="<?= trans('com_pagebuilder_page_remove'); ?>" css="red" confirm="<?= trans('com_pagebuilder_page_remove_confirm'); ?>"></pb-toolbar-item>
        </pb-toolbar>
        <template>
			<h1><?= trans('com_pagebuilder_page_update'); ?></h1>
			<pb-body-page id="<?= request()->get('itemid', ''); ?>"></pb-body-page>
        </template>
    </pb-body>
</div>