<?
    toolbar()->title(
        trans('com_pagebuilder_pages')
    );
?>
<div class="pb-reset grid-flex" data-nano="true" data-backend="true">
    <pb-sidebar>
        <? include(JPATH_COMPONENT . '/view/menu.php'); ?>
    </pb-sidebar>
    <pb-body>
        <pb-toolbar slot="toolbar">
            <pb-toolbar-item link="<?= route('index.php?option=com_pagebuilder&view=page&task=create') ?>" icon="icon-plus" value="<?= trans('com_pagebuilder_page_add'); ?>" css="green"></pb-toolbar-item>
        </pb-toolbar>
        <template slot="filter">
            <pb-filter></pb-filter>
        </template>
        <template>
            <h1><?= trans('com_pagebuilder_pages'); ?></h1>
            <pb-filter-body v-model="$store.state.Page.items"></pb-filter-body>
        </template>
    </pb-body>
</div>