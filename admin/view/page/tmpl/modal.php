<?
    toolbar()->title(
        trans('com_pagebuilder_pages')
    );
?>
<div class="pb-reset grid-flex" data-nano="true" data-backend="true">
    <pb-body>
        <template slot="filter">
            <pb-filter :modal="true"></pb-filter>
        </template>
        <template>
            <h2><?= trans('com_pagebuilder_pages'); ?></h2>
            <pb-filter-body v-model="$store.state.Page.items" :modal="true" modal-function="selectPage"></pb-filter-body>
        </template>
    </pb-body>
</div>