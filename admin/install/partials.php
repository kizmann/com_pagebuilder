<?php

use Illuminate\Database\Capsule\Manager as DB;

if ( DB::schema()->hasTable('pagebuilder_partials') ) {
    return;
}

DB::schema()->create('pagebuilder_partials', function ($table) {

    $table
        ->increments('id')
        ->unique();

    $table
        ->integer('state')
        ->default(0);

    $table
        ->string('lang')
        ->default('');
    
    $table
        ->text('category')
        ->default('');

    $table
        ->string('title')
        ->default('');

    $table
        ->text('body')
        ->default('');

    $table->timestamps();
});