<?php

namespace Pagebuilder\Model;

use Nano\Component\Model as Model;

class MenuType extends Model
{

    public $table = 'menu_types';

    public $guarded = [
        'id'
    ];

    public function menuBackend()
    {
        return $this->hasMany('Pagebuilder\Model\Menu', 'menutype', 'menutype')->where('parent_id', 1)->orderBy('rgt', 'asc')->with(['menuBackend']);
    }

    public function menuFrontend()
    {
        return $this->hasMany('Pagebuilder\Model\Menu', 'menutype', 'menutype')->where('parent_id', 1)->orderBy('rgt', 'asc')->with(['menuFrontend']);
    }

    public function root()
    {
        return $this->hasMany('Pagebuilder\Model\Menu', 'menutype', 'menutype')->where('parent_id', 1);
    }

}