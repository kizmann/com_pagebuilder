<?php

namespace Pagebuilder\Model;

jimport('joomla.html.html');

use Nano\Component\Model as Model;
use Pagebuilder\Helper\State as StateHelper;
use Pagebuilder\Helper\Lang as LangHelper;

use JFactory as JFactory;
use JText as JText;
use JHtml as JHtml;

class Partial extends Model
{

    public $table = 'pagebuilder_partials';

    public $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    public $fillable = [
        'state', 'lang', 'title', 'body'
    ];

    public $appends = [
        'stateObject', 'langObject', 'updateLink', 'showLink'
    ];

    public $attributes = [
        'state'     => 0,
        'lang'      => '*',
        'title'     => '',
        'body'      => '{ "partial": [] }'
    ];

    public static $rules = [
        'title' => 'required',
        'state'	=> 'required',
        'lang'	=> 'required'
    ];

    public static $customMessages = [
        '*' => 'com_pagebuilder_error_:attribute'
    ];

    public function getUpdateLinkAttribute()
    {
        if ( ! isset($this->attributes['id']) ) {
            return '';
        }

        return 'index.php?option=com_pagebuilder&view=partial&task=update&itemid=' . $this->attributes['id'];
    }

    public function getShowLinkAttribute()
    {
        if ( ! isset($this->attributes['id']) ) {
            return '';
        }

        return 'index.php?option=com_pagebuilder&view=partial&task=show&itemid=' . $this->attributes['id'];
    }

    public function getStateObjectAttribute()
    {
        $state = StateHelper::make()->find($this->attributes['state']);

        if ( ! $state ) {
            return [];
        }

        return $state;
    }

    public function getLangObjectAttribute()
    {
        $lang = LangHelper::make()->find($this->attributes['lang']);

        if ( ! $lang ) {
            return [];
        }

        return $lang;
    }

    public function getCreatedAtAttribute($value)
    {
        return JHtml::date($value, 'd.m.Y');
    }

    public function getUpdatedAtAttribute($value)
    {
        return JHtml::date($value, 'd.m.Y');
    }

    public function setBodyAttribute($value)
    {
        if ( is_string($value) ) {
            $value = json_decode($value);
        }

        $this->attributes['body'] = json_encode($value);
    }

    public function getBodyAttribute($value)
    {
        $object = json_decode($value);

        if ( ! $value || ! is_object($object) ) {
            $object = new \StdClass;
        }

        if ( ! property_exists($object, 'partial') ) {
            $object->partial = [];
        }

        return $object;
    }

}