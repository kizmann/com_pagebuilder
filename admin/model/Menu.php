<?php

namespace Pagebuilder\Model;

use Nano\Component\Model as Model;
use Pagebuilder\Helper\State as StateHelper;

class Menu extends Model
{

	public $table = 'menu';

	public $guarded = [
		'id'
	];

	public $appends = [
		'stateObject', 'activeMenu'
	];

	public function menutype()
	{
		return $this->hasOne('Pagebuilder\Model\MenuType', 'menutype', 'menutype');
	}

	public function parent()
	{
		return $this->hasOne('Pagebuilder\Model\Menu', 'id', 'parent_id');
	}

	public function menuBackend()
	{
		return $this->hasMany('Pagebuilder\Model\Menu', 'parent_id', 'id')
		->orderBy('lft', 'asc')->with(['menuBackend']);
	}

	public function menuFrontend()
	{
		return $this->hasMany('Pagebuilder\Model\Menu', 'parent_id', 'id')
		->where('published', 1)->where('access', '<', user()->guest ? 2 : 3)->where('params', 'LIKE', '%"menu_show":1%')->orderBy('lft', 'asc')->with(['menuFrontend']);
	}

	public function getParamsAttribute()
	{
		if ( isset($this->attributes['params']) === false ) {
			return [];
		}

		return json_decode($this->attributes['params']);
	}

	public function getActiveMenuAttribute()
	{
		$active = app()->getMenu()->getActive();

		if ( ! $active || ! $this->attribute['id'] ) {
			return 0;
		}

		return toInteger($active) === $this->attribute['id'];
	}

	public function getStateObjectAttribute()
	{
		if ( isset($this->attributes['published']) === false ) {
			return [];
		}

		$state = StateHelper::make()->find($this->attributes['published']);

		if ( ! $state ) {
			return [];
		}

		return $state;
	}

}