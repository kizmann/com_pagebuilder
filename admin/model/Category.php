<?php

namespace Pagebuilder\Model;

use Nano\Component\Model as Model;

class Category extends Model
{

    public $table = 'categories';

    public $guarded = [
        'id'
    ];
    
}