<?php

namespace Pagebuilder\Model;

jimport('joomla.html.html');

use Nano\Component\Model as Model;
use Pagebuilder\Model\Category as CategoryModel;
use Pagebuilder\Helper\State as StateHelper;
use Pagebuilder\Helper\Lang as LangHelper;
use JHtml as JHtml;

class Page extends Model
{

    public $table = 'pagebuilder_pages';

    public $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    public $fillable = [
        'state', 'lang', 'category', 'title', 'body'
    ];

    public $appends = [
        'categories', 'stateObject', 'langObject', 'updateLink', 'showLink', 'fullUpdatedAt', 'fullUpdatedAt'
    ];

    public $attributes = [
        'state'     => 0,
        'lang'      => '*',
        'title'     => '',
        'body'      => '{ "content": [], "sidebar": [], "footer": [] }'
    ];

    public static $rules = [
        'title' => 'required',
        'state'	=> 'required',
        'lang'	=> 'required'
    ];

    public static $customMessages = [
        '*' => 'com_pagebuilder_error_:attribute'
    ];

    public function getCategoriesAttribute()
    {
        return (new CategoryModel)->whereIn('id', $this->category)->orderBy('lft', 'asc')->get();
    }

    public function getUpdateLinkAttribute()
    {
        if ( ! isset($this->attributes['id']) ) {
            return '';
        }

        return 'index.php?option=com_pagebuilder&view=page&task=update&itemid=' . $this->attributes['id'];
    }

    public function getShowLinkAttribute()
    {
        if ( ! isset($this->attributes['id']) ) {
            return '';
        }

        return 'index.php?option=com_pagebuilder&view=page&itemid=' . $this->attributes['id'];
    }
    
    public function getStateObjectAttribute()
    {
        $state = StateHelper::make()->find($this->attributes['state']);

        if ( ! $state ) {
            return [];
        }

        return $state;
    }

    public function getLangObjectAttribute()
    {
        $lang = LangHelper::make()->find($this->attributes['lang']);

        if ( ! $lang ) {
            return [];
        }

        return $lang;
    }

    public function getCreatedAtAttribute($value)
    {
        return JHtml::date($value, 'd.m.Y');
    }

    public function getFullCreatedAtAttribute($value)
    {
        return JHtml::date($value, 'd.m.Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return JHtml::date($value, 'd.m.Y');
    }

    public function getFullUpdatedAtAttribute($value)
    {
        return JHtml::date($value, 'd.m.Y H:i:s');
    }

    public function setBodyAttribute($value)
    {
        if ( is_string($value) ) {
            $value = json_decode($value);
        }

        $this->attributes['body'] = json_encode($value);
    }

    public function getBodyAttribute($value)
    {
        $object = json_decode($value);

        if ( ! $value || ! is_object($object) ) {
            $object = new \StdClass;
        }

        if ( ! property_exists($object, 'content') ) {
            $object->content = [];
        }

        if ( ! property_exists($object, 'sidebar') ) {
            $object->sidebar = [];
        }

        if ( ! property_exists($object, 'footer') ) {
            $object->footer = [];
        }

        return $object;
    }

    public function setCategoryAttribute($value)
    {
        if ( is_string($value) ) {
            $value = json_decode($value);
        }

        $this->attributes['category'] = json_encode($value);
    }

    public function getCategoryAttribute($value)
    {
        $array = json_decode($value);

        if ( ! $value || ! is_array($array) ) {
            $array = [];
        }

        return $array;
    }

}