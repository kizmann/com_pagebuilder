<?
    $empty = toObject([
        'content' => @ count($item->body->content) === 0,
        'sidebar' => @ count($item->body->sidebar) === 0,
        'footer' => @ count($item->body->footer) === 0
    ]);

    $sidebar = doc()->getBuffer('modules', 'sidebar', ['style' => 'none']);
?>

<div id="pagebuilder" class="grid-flex grid-spaced">
    <? if ( ! $empty->content ) : ?>
        <div class="pagebuilder-content <?= $empty->sidebar && ! $sidebar && @ app()->getMenu()->getActive()->home === '1' ? 'col-12-12' : 'col-8-12 col-xl-12-12'; ?>">
            <? if ( app()->getMenu()->getActive() && app()->getParams()->get('show_page_heading') ) : ?>
                <div class="page-header">
                    <h1><?= app()->getMenu()->getActive()->title; ?></h1>
                </div>
            <? endif; ?>
            <?
                foreach ($item->body->content as $element) {
                    Pagebuilder\Helper\Config::make()->render($element->type, ['element' => $element, 'position' => 'content']);
                }
            ?>
        </div>
    <? endif; ?>
    <? if ( ! $empty->sidebar || $sidebar ) : ?>
        <div class="pagebuilder-sidebar col-4-12 col-xl-12-12">
            <?= $sidebar; ?>
            <?
                foreach ($item->body->sidebar as $element) {
                    Pagebuilder\Helper\Config::make()->render($element->type, ['element' => $element, 'position' => 'sidebar']);
                }
            ?>
        </div>
    <? endif; ?>
    <? if ( ! $empty->footer ) : ?>
        <div class="pagebuilder-footer col-12-12">
            <div class="fill-white">
                <?
                    foreach ($item->body->footer as $element) {
                        Pagebuilder\Helper\Config::make()->render($element->type, ['element' => $element, 'position' => 'footer']);
                    }
                ?>
            </div>
        </div>
    <? endif; ?>
</div>
