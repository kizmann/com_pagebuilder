<?php

if( ! defined('NANO_VERSION') || NANO_VERSION < 2 ) {
    throw new Exception('Nano Framework version 2.x is required.');
}

use Nano\Component\Loader as Loader;
use Nano\Component\Provider as Provider;
use Nano\Component\Factory as Factory;

(new Loader)->addPrefix('Pagebuilder', JPATH_COMPONENT_ADMINISTRATOR)->register();
(new Factory)->boot(['pagebuilder'], ['page']);